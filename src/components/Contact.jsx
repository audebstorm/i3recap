import style from '../styles/contact/contact.module.css'

const Contact = (props) => {
    const { firstname, lastname, email } = props

    return (
        <div className={style.contact}>
            <div className={style.label}>{firstname}</div>
            <div className={style.label}>{lastname}</div>
            <div className={style.label}>{email}</div>
        </div>
    )
}

export default Contact