import styleStudent from '../styles/card/student-card.module.css'
import styleTrainer from '../styles/card/trainer-card.module.css'

const TrainerCard = (props) => {
    const { firstname, lastname, avatar, vacation, project, onTrainerSelect } = props
    //onTrainerSelect sera un callback, pour le différencier de nos propriétés, on le nomme souvent avec on devant plus un nom qui ressemble à un nom d'event

    const handleTrainerSelect = () => {
        onTrainerSelect(firstname)
    }

    if(project){
        return (
            <div className={styleTrainer.card}>
                <h2> {firstname} est sur projet </h2>
            </div>
        )
    }
    {/* le else n'est pas obligatoire, si on n'est pas rentré dans le if, on passera forcément par ici */}
    return (
        <div className={styleTrainer.card}>
            <img className={styleTrainer.imgcard} src={avatar} alt={"avatar de " + firstname} />
            <h2>{firstname} {lastname}</h2>
            { (vacation) ? 
                <button className={styleTrainer.button} disabled>Pas disponible</button>
             :
                // Au click, on déclenche une méthode qui va déclencher l'évènement reçu dans les props en lui donnant en paramètre le prénom reçu via les props
                <button className={styleTrainer.button} onClick={handleTrainerSelect}>Sélectionner</button>
            }
            
            {/* (!vacation) && <button>Sélectionner</button> */}
            {/* && -> Si vacation est faux, crée le bouton */}
            {/* (vacation) ?? <button>Sélectionner</button> */}
            {/* ?? -> Si vacation vaut null, crée le bouton */}
            {/* (vacation) || <button>Sélectionner</button> */}
            {/* || -> Si vacation vaut faux, null ou undefined, crée le bouton */}
             
        </div>
    )
}

const StudentCard = (props) => {
    const { lastname, firstname, avatar } = props

    return (
        <div className={styleStudent.card}>
            <img className={styleStudent.imgcard} src={avatar} alt={"avatar de " + {firstname}} />
            <h2> {firstname} {lastname} </h2> {/* binding */}
        </div>
    )
}

export { TrainerCard, StudentCard }