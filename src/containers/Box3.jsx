import { useState } from "react"
import { useForm } from "react-hook-form"
import Contact from "../components/Contact"
import style from "../styles/box/box3.module.css"

const Box3 = () => {
    const { register, handleSubmit, formState : { errors } } = useForm({ mode : "onChange" })
    //register -> ce qui nous permet d' ""enregistrer"" chaque champs dans une propriété des datas
    //handleSubmit -> fonction qu'on va déclencher au submit (qui contient déjà le preventdefault)
    //formState : { errors } -> pour jouer avec la gestion d'erreurs

    const [contacts, setContacts] = useState([
        { firstname : "Aude", lastname : "Beurive", email : "aude.beurive@bstorm.be"},
        { firstname : "Aurélien", lastname : "Strimelle", email : "aurelien.strimelle@bstorm.be"}
    ])

    // const contacts = [
    //     { firstname : "Aude", lastname : "Beurive", email : "aude.beurive@bstorm.be"},
    //     { firstname : "Aurélien", lastname : "Strimelle", email : "aurelien.strimelle@bstorm.be"}
    // ]

    const addContact = (data) => {
        //contacts.push(data)
        setContacts(previous => [...previous, data])
        console.log(contacts);
    }

    return (
        <div className={style.container}>
            <div>
                <form onSubmit={handleSubmit(addContact)}>
                    <div>
                        <label htmlFor="firstame">Prénom : </label>
                        <input id='firstname' type="text" {...register("firstname", { required : true })} />
                        { errors.firstname?.type === "required" && <span>Ce champs est requis</span> }
                    </div>
                    <div>
                        <label htmlFor="lastname">Nom : </label>
                        <input id="lastname" type="text" {...register("lastname", { required : true })}/>
                        { errors.lastname?.type === "required" && <span>Ce champs est requis</span> }
                    </div>
                    <div>
                        <label htmlFor="email">Email : </label>
                        <input id="email" type="email" {...register("email", { required : true})}/>
                        { errors.email?.type === "required" && <span>Ce champs est requis</span> }
                    </div>
                    <input type="submit" value="Créer"/>
                </form>
            </div>

            <div className={style.contacts}>
                { contacts.map((c, index) => <Contact key={index} firstname={c.firstname} lastname={c.lastname} email={c.email} />) }
            </div>
        </div>
    )
}

export default Box3