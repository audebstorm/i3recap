//Première façon d'apporter du style -> un simple fichier css
import "../styles/hello/hello.css"
//Deuxième façon d'apporter du style -> un fichier module.css
import style from "../styles/hello/hello.module.css"

//Création d'un componant const sonPtitNom = () => {}
const Hello = () => {

    //Rendu html du componant
    return (
        <div className={style.container}>
            <h1 className="title">Hello World</h1>
        </div>
    )
}

export default Hello
