import { useState } from "react"
import { TrainerCard } from "../components/Card"
import style from "../styles/box/box2.module.css"

const Box2 = () => {
    const [nbClick, setNbClick] = useState(0)
    const [trainerOfTheDay, setTrainer] = useState("[...en attente...]")

    const formateur = {
        id : 1,
        firstname : "Aude",
        lastname : "Beurive",
        avatar : "/images/aude.png",
        vacation : true,
        project : false
    }

    const formateurs = [ 
        { id : 1, firstname : "Aude", lastname : "Beurive", avatar : "/images/aude.png", vacation : false, project : false },
        { id : 2, firstname : "Aurélien", lastname : "Strimelle", avatar : "/images/aurelien.png", vacation : false, project : false },
        { id : 3, firstname : "Khun", lastname : "Ly", avatar : "/images/khun.png", vacation : true, project : false },
        { id : 4, firstname : "Gavin", lastname : "Chaineux", avatar : "/images/gavin.png", vacation : false, project : true } 
    ]

    //let nbClick = 0;
    const touch = () => {
        //nbClick++
        setNbClick(previousValue => previousValue + 1)
        console.log(`Touché ${nbClick} fois`)
    }

    const changeTrainer = (firstnameTrainer) => {
        setTrainer(firstnameTrainer) //On modifie la valeur de tainerOfTheDay pour qu'il prenne le prénom fourni en paramètre
    }

    return(
        <div>
            <div className={style.count}>
                {/* <div>Touché {nbClick} fois !</div>
                <button onClick={touch}>Click me !</button> */}
                <p>Le formateur du jour est {trainerOfTheDay}</p>
            </div>            
            <div className={style.container}>
                {/* <TrainerCard firstname={formateur.firstname} lastname={formateur.lastname} avatar={formateur.avatar} vacation={formateur.vacation} project={formateur.project}/> */}
                { formateurs.map(f => <TrainerCard key={f.id} firstname={f.firstname} lastname={f.lastname} avatar={f.avatar} vacation={f.vacation} project={f.project} onTrainerSelect={changeTrainer} /> ) }
            </div>
        </div>
    )
} 

export default Box2