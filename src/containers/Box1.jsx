import { StudentCard } from "../components/Card"

import style from "../styles/box/box1.module.css"

const Box1 = () => {

    const student = { id : 1, firstname : "Robert", lastname : "Dupond", avatar : "/images/robert.png" }

    return (
        <div className={style.container}>
            <StudentCard firstname={student.firstname} lastname={student.lastname} avatar={student.avatar}/>
        </div>
    )
}

export default Box1