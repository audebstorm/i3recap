import logo from './logo.svg';
import './App.css';
import Hello from './containers/Hello';
import Box1 from './containers/Box1';
import Box2 from './containers/Box2';
import Box3 from './containers/Box3';


function App() {
  return (
    <div>
      {/* Pour insérer notre composant : */}
      <Hello />
      <Box1 />
      <Box2 />
      <Box3 />
    </div>
  );
}

export default App;
